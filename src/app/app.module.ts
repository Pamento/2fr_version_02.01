import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app.routing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';

import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { MatTabsModule } from '@angular/material/tabs';
import { MatFormFieldModule } from '@angular/material';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatDividerModule } from '@angular/material/divider';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/pages/home/home.component';
import { CoursesComponent } from './components/pages/courses/courses.component';
import { LoginComponent } from './components/pages/login/login.component';
import { MenuComponent } from './components/partials/menu/menu.component';
import { FormLoginComponent } from './components/partials/form-login/form-login.component';
import { FormSignupComponent } from './components/partials/form-signup/form-signup.component';
import { ScheduleComponent } from './schedule/schedule.component';
import { FormContactComponent } from './components/pages/form-contact/form-contact.component';

import { ComparValidatorDirective } from './shered/compar-validator.directive';
import { FormResetpasswordComponent } from './components/partials/form-resetpassword/form-resetpassword.component';
import { AuthService } from './services/auth.service';
import { DataService } from './services/data.service';
import { AuthGuard } from './auth/auth.guard';
import { HomeFooterComponent } from './components/partials/home-footer/home-footer.component';
import { NewsComponent } from './components/pages/news/news.component';
import { MenuInternalComponent } from './components/partials/menu-internal/menu-internal.component';
import { ResourcesComponent } from './components/pages/resources/resources.component';
import { FaqComponent } from './components/pages/faq/faq.component';
import { TestComponent } from './components/pages/test/test.component';
import { AdministrationComponent } from './components/pages/administration/administration.component';
import { LearnerListComponent } from './components/partials/admin-dashboard/learners-list/learner-list.component';
import { LearnerTestComponent } from './components/partials/admin-dashboard/learner-test/learner-test.component';
import { LearnerDetailComponent } from './components/partials/admin-dashboard/learner-detail/learner-detail.component';
import { LearnerProfilComponent } from './components/pages/learner-profil/learner-profil.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CoursesComponent,
    LoginComponent,
    MenuComponent,
    ComparValidatorDirective,
    FormLoginComponent,
    FormSignupComponent,
    ScheduleComponent,
    FormContactComponent,
    FormResetpasswordComponent,
    HomeFooterComponent,
    NewsComponent,
    MenuInternalComponent,
    ResourcesComponent,
    FaqComponent,
    TestComponent,
    AdministrationComponent,
    LearnerListComponent,
    LearnerTestComponent,
    LearnerDetailComponent,
    LearnerProfilComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    ScrollToModule.forRoot(),
    MatInputModule,
    MatFormFieldModule,
    MatSelectModule,
    MatButtonModule,
    MatCheckboxModule,
    MatChipsModule,
    MatIconModule,
    MatTabsModule,
    MatExpansionModule,
    HttpClientModule,
    MatDividerModule
  ],
  providers: [DataService, AuthService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
