export const CheckTest = [
  {
    "id": "question_01",
    "question": "s’engager",
    "answer": ["to engage one’s self"]
  },
  {
    "id": "question_02",
    "question": "épouser",
    "answer": ["to marry"]
  },
  {
    "id": "question_03",
    "question": "par hasard",
    "answer": ["by accident"]
  },
  {
    "id": "question_04",
    "question": "faire la connaissance de",
    "answer": ["meet somebody"]
  },
  {
    "id": "question_05",
    "question": "se fâcher contre",
    "answer": ["to be mad at"]
  },
  {
    "id": "question_06",
    "question": "il se peut que",
    "answer": ["maybe", "I suppose"]
  },
  {
    "id": "question_07",
    "question": "en avoir marre",
    "answer": ["to be sick of", "to forget"]
  },
  {
    "id": "question_08",
    "question": "de nouveau",
    "answer": ["again"]
  },
  {
    "id": "question_09",
    "question": "manquer",
    "answer": ["to miss"]
  },
  {
    "id": "question_10",
    "question": "être en train de",
    "answer": ["in the act of"]
  },
  {
    "id": "question_11",
    "question": "près de",
    "answer": ["near"]
  },
  {
    "id": "question_12",
    "question": "éclater de rire",
    "answer": ["laugh in hysterics"]
  },
  {
    "id": "question_13",
    "question": "de cette façon",
    "answer": ["this way"]
  },
  {
    "id": "question_14",
    "question": "changer d’avis",
    "answer": ["change opinion"]
  },
  {
    "id": "question_15",
    "question": "cela n’importe",
    "answer": ["it is not important"]
  },
  {
    "id": "question_16",
    "question": "être dans son assiette",
    "answer": ["to be in a good mood"]
  },
  {
    "id": "question_17",
    "question": "faire des châteaux en Espagne",
    "answer": ["to dream of having wild projects"]
  },
  {
    "id": "question_18",
    "question": "et ainsi de suite",
    "answer": ["finally"]
  },
  {
    "id": "question_19",
    "question": "saisir",
    "answer": ["to grab", "seize", "understand", "catch"]
  },
  {
    "id": "question_20",
    "question": "de temps à autre",
    "answer": ["another time"]
  },
  {
    "id": "question_21",
    "question": "mieux vaut tard que jamais",
    "answer": ["better late than never"]
  },
  {
    "id": "question_22",
    "question": "faire des progrès",
    "answer": ["to make progress"]
  },
  {
    "id": "question_23",
    "question": "faire semblant de",
    "answer": ["to pretend"]
  },
  {
    "id": "question_24",
    "question": "faire exprès",
    "answer": ["intentionally"]
  },
  {
    "id": "question_25",
    "question": "envoyer chercher",
    "answer": ["to send out for"]
  },
  {
    "id": "question_26",
    "question": "apprendre par cœur",
    "answer": ["learn by heart"]
  },
  {
    "id": "question_27",
    "question": "être amoureux de",
    "answer": ["to be in love with"]
  },
  {
    "id": "question_28",
    "question": "exiger",
    "answer": ["to demand", "require"]
  },
  {
    "id": "question_29",
    "question": "par exemple",
    "answer": ["for example"]
  },
  {
    "id": "question_30",
    "question": "en tout cas",
    "answer": ["in any case"]
  },
  {
    "id": "question_31",
    "question": "enfin de compte",
    "answer": ["the end of the"]
  },
  {
    "id": "question_32",
    "question": "rendre visite à",
    "answer": ["to visit someone"]
  },
  {
    "id": "question_33",
    "question": "avoir l’occasion de",
    "answer": ["to have the opportunity"]
  },
  {
    "id": "question_34",
    "question": "malgré cela",
    "answer": ["however", "still", "never the less"]
  },
  {
    "id": "question_35",
    "question": "se souvenir de",
    "answer": ["to remember", "retrieve"]
  },
  {
    "id": "question_36",
    "question": "sans doute",
    "answer": ["without a doubt", "doubtlessly"]
  },
  {
    "id": "question_37",
    "question": "au lieu de",
    "answer": ["instead of", "in place of"]
  },
  {
    "id": "question_38",
    "question": "à cause de",
    "answer": ["because of"]
  },
  {
    "id": "question_39",
    "question": "à peu près",
    "answer": ["more or less", "almost", "about", "nearly"]
  },
  {
    "id": "question_40",
    "question": "être au courant",
    "answer": ["beware"]
  },
  {
    "id": "question_41",
    "question": "faire la sourde oreille",
    "answer": ["ignoring a request"]
  },
  {
    "id": "question_42",
    "question": "l’espérance",
    "answer": ["hope"]
  },
  {
    "id": "question_43",
    "question": "cela m’est égal",
    "answer": ["I don’t care"]
  },
  {
    "id": "question_44",
    "question": "à tout prix",
    "answer": ["at any cost"]
  },
  {
    "id": "question_45",
    "question": "un cauchemar",
    "answer": ["nightmare"]
  },
  {
    "id": "question_46",
    "question": "faire la guerre",
    "answer": ["to battle", "to fight for something"]
  },
  {
    "id": "question_47",
    "question": "effrayer",
    "answer": ["to frighten", "alarm", "put off"]
  },
  {
    "id": "question_48",
    "question": "à mon gré",
    "answer": ["the way I like"]
  },
  {
    "id": "question_49",
    "question": "faire la queue",
    "answer": ["to wait in line"]
  },
  {
    "id": "question_50",
    "question": "disponible/libre",
    "answer": ["available", "free"]
  },
  {
    "id": "question_51",
    "question": "le mensonge/la vérité",
    "answer": ["lie/truth", "lie truth", "lie, truth"]
  },
  {
    "id": "question_52",
    "question": "mépriser/détester",
    "answer": ["to scorn/to detest", "to scorn/loathe", "to scorn/hate"]
  },
  {
    "id": "question_53",
    "question": "la louange",
    "answer": ["praise"]
  },
  {
    "id": "question_54",
    "question": "épargner",
    "answer": ["to save", "spare"]
  },
  {
    "id": "question_55",
    "question": "l’ampleur de la tâche",
    "answer": ["scale of the task"]
  },
  {
    "id": "question_56",
    "question": "un sondage",
    "answer": ["poll", "survey"]
  },
  {
    "id": "question_57",
    "question": "un échantillon",
    "answer": ["sample", "example"]
  },
  {
    "id": "question_58",
    "question": "la polémique",
    "answer": ["controversy"]
  },
  {
    "id": "question_59",
    "question": "l’enquête",
    "answer": ["investigation", "inquiry"]
  },
  {
    "id": "question_60",
    "question": "avoir le cœur gros",
    "answer": ["to be sad"]
  },
  {
    "id": "question_61",
    "question": "pluvieux",
    "answer": ["wet", "rainy"]
  },
  {
    "id": "question_62",
    "question": "embouteillage",
    "answer": ["traffic jam"]
  },
  {
    "id": "question_63",
    "question": "à l’époque où nous sommes",
    "answer": ["at the time where we are"]
  },
  {
    "id": "question_64",
    "question": "de temps en temps",
    "answer": ["from time to time", "occasionally"]
  },
  {
    "id": "question_65",
    "question": "en moyenne",
    "answer": ["on average"]
  }
]