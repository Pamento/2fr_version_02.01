import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { DataService } from '../services/data.service';


@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {

  constructor(
    private dataService: DataService,
    private Router: Router
  ) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if ((typeof this.dataService.admin !== 'undefined') || (this.dataService.admin !== undefined)) {
      if (this.dataService.admin.isAdmin) {
        return true;
      } else {
        this.Router.navigate(['/home']);
        return false;
      }
    } else {
      this.dataService.getAdmin();
    }
  }
}
