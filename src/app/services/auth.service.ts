import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from './data.service';
import { Learner } from '../entities/learner';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  learner: Learner;
  constructor(
    private Router: Router,
    private dataBase: DataService
  ) { }

  getToken() {
    return JSON.parse(localStorage.getItem('token'));
  }
  getAdmin() {
    return JSON.parse(localStorage.getItem('admin'));
  }
  isLoggedIn() {
    return this.getToken() !== null;
  }
  isAdmin() {
    return this.getAdmin() !== null;
  }
  getId() {
    return JSON.parse(localStorage.getItem('learner')).userId;
  }
  logout() {
    localStorage.removeItem('token');
    this.Router.navigate(['/login/login-form']);
  }
}
