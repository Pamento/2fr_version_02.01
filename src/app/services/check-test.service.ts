import { Injectable } from '@angular/core';
import { CheckTest } from '../helpers/correct-test';

@Injectable({
  providedIn: 'root'
})
export class CheckTestService {

  private modelTest: Array<Object> = CheckTest;
  public score: any = 0;

  constructor() { }

  doCheck(test) {
    for (const key in test) {
      if (test.hasOwnProperty(key)) {
        const e = test[key];
        this.compare(e, key);
      } else { continue; }
    }
    return this.score;
  }
  compare(answer, key) {
    // we get answer value from modelTest object
    function byId(question) {
        return question.id === key;
    }
    let modelAnswer = this.modelTest.find(byId);
    let _a: Array<string> = modelAnswer['answer'];
    for (const _v of _a) {
      if (_v === answer) {
        this.score += 1;
      }
    }
  }
}
