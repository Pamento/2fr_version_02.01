import { TestBed, inject } from '@angular/core/testing';

import { CheckTestService } from './check-test.service';

describe('CheckTestService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CheckTestService]
    });
  });

  it('should be created', inject([CheckTestService], (service: CheckTestService) => {
    expect(service).toBeTruthy();
  }));
});
