import { TestBed, inject } from '@angular/core/testing';

import { FrenchTestService } from './french-test.service';

describe('FrenchTestService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FrenchTestService]
    });
  });

  it('should be created', inject([FrenchTestService], (service: FrenchTestService) => {
    expect(service).toBeTruthy();
  }));
});
