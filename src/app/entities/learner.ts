/**
 * @param avatar it must by Buffer, the Buffer is expected by the LoopBack Server
 */
export class Learner {
  name: string;
  surname?: string;
  email: string;
  password: string;
  confirmPassword: string;
  created: Date;
  test: boolean;
  coursesCounter?: number;
  last_modification: Date;
  isAdmin: boolean;
  avatar?: string;
  comment?: string;
  id?: number;
}
export class LearnerComments {
  level: string;
  formula: string;
  coursesCounter: number;
  comments: string;
}
