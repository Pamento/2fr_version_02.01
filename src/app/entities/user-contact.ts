export class UserContact {
  name: string;
  email: string;
  password: string;
  confirmEmail: string;
  message: string;
}