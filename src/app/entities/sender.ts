export class Sender {
  to: string;
  from: string;
  subject: string;
  text: string;
  html: string;
}