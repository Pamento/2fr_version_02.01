export class Comment {
    idComment: number;
    idUser: number;
    idPost: number;
    comment: string;
    approved: boolean;
}