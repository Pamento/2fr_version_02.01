export class Test {
  idUser: Number;
  created: Date;
  result: string;
  comment: string;
  answers: string;
}
export class TestResult {
  correctAnswer: number;
  emptyAnswer: number;
  total: number;
}