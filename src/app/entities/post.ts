export class Post {
    idPost: number;
    idUser: number;
    post: string;
    created: Date;
    last_modification: Date;
    author: string;
    categories: string;
    hasComments: boolean;
}