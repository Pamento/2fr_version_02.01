import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/pages/home/home.component';
import { CoursesComponent } from './components/pages/courses/courses.component';
import { ResourcesComponent } from './components/pages/resources/resources.component';
import { NewsComponent } from './components/pages/news/news.component';
import { FaqComponent } from './components/pages/faq/faq.component';
import { LoginComponent } from './components/pages/login/login.component';
import { FormLoginComponent } from './components/partials/form-login/form-login.component';
import { FormSignupComponent } from './components/partials/form-signup/form-signup.component';
import { TestComponent } from './components/pages/test/test.component';
import { ScheduleComponent } from './schedule/schedule.component';
import { FormContactComponent } from './components/pages/form-contact/form-contact.component';
import { FormResetpasswordComponent } from './components/partials/form-resetpassword/form-resetpassword.component';
import { AuthGuard } from './auth/auth.guard';
import { AdminGuard } from './auth/admin.guard'
import { AdministrationComponent } from './components/pages/administration/administration.component';
import { LearnerListComponent } from './components/partials/admin-dashboard/learners-list/learner-list.component';
import { LearnerTestComponent } from './components/partials/admin-dashboard/learner-test/learner-test.component';
import { LearnerDetailComponent } from './components/partials/admin-dashboard/learner-detail/learner-detail.component';
import { LearnerProfilComponent } from './components/pages/learner-profil/learner-profil.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'contact', component: FormContactComponent },
  { path: 'frenchCourses', component: CoursesComponent },
  { path: 'resources', component: ResourcesComponent },
  { path: 'news', component: NewsComponent },
  { path: 'faq', component: FaqComponent },
  {
    path: 'daschboard', component: AdministrationComponent, canActivate: [AdminGuard],
    children: [
      { path: '', redirectTo: 'loginForm', pathMatch: 'full' },
      { path: 'learner-list', component: LearnerListComponent },
      { path: 'learner-test', component: LearnerTestComponent },
      { path: 'learner', component: LearnerDetailComponent }
    ]
  },
  { path: 'schedule', component: ScheduleComponent, canActivate: [AuthGuard] },
  { path: 'my-profile', component: LearnerProfilComponent, canActivate: [AuthGuard] },
  {
    path: 'login', component: LoginComponent,
    children: [
      { path: '', redirectTo: 'loginForm', pathMatch: 'full' },
      { path: 'login-form', component: FormLoginComponent },
      { path: 'signup-form', component: FormSignupComponent },
      { path: 'reset', component: FormResetpasswordComponent }
    ]
  },
  { path: 'test', component: TestComponent },
  { path: '**', component: HomeComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    useHash: true,
    anchorScrolling: 'enabled',
    scrollPositionRestoration: 'enabled'
  })],
  exports: [RouterModule],
})
export class AppRoutingModule { }

export const routingComponents = [
  HomeComponent
];
