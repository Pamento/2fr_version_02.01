import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { DataService } from '../../../services/data.service';
import { Post } from '../../../entities/post';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {

  posts: Array<Post> = [];
  newPost: Post;
  isLoggedIn: boolean;
  newsForm: FormGroup;
  author: any;
  isAdmin: boolean = false;
  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private dataService: DataService
  ) { }

  /*
   * ngOnInit() use atuhService for allowed or denny access to
   * articles editor
   */
  ngOnInit() {
    if (this.authService.isAdmin()) {
      this.isAdmin = true;
    } else { this.isAdmin = false }
    this.dataService.getPosts().subscribe(
      data => {
        this.posts = data;
        for (const i of this.posts) {
          i.post = JSON.parse(i.post);
        }
      },
      err => { console.log('Error for post page!', err) }
    )
    this.newPostForm();
  }
  newPostForm() {
    return this.newsForm = this.fb.group({
      title: [null, Validators.required],
      post: [null, Validators.required]
    });
  }
  get title() {
    return this.newsForm.get('title');
  }
  get post() {
    return this.newsForm.get('post');
  }
  submit() {
    console.log('Ok');
    this.author = this.dataService.admin;
    if (this.newsForm.valid) {
      const newPost = Object.assign({});
      newPost.post = JSON.stringify(this.newsForm.value);
      newPost.idPost = this.author.id
      newPost.idUser = this.author.id;
      newPost.created = new Date();
      newPost.last_modification = new Date();
      newPost.author = this.author.name;
      newPost.categories = 'article';
      this.dataService.addPost(newPost)
        .subscribe(
          (data: any) => {
            this.posts.push(newPost);
          },
          err => { console.log('Error in News add Post !\n', err) }
        );
    } else {null}
  }
  deletePost($event, id: number) {
    if (confirm('Cette action va suprimer DEFINITIVEMENT\n' +
      'tout les données de la BASE DE DONNÉES!\n\n' +
      'Êtes-vous sur de vouloire\n' +
      'SUPRIMER l\'apprenti du régistre ?')) {

      this.dataService.deletePost(id).subscribe(
        res => { console.log('response from deletePost', res); },
        err => { console.error('Error in news page !', err); }
      );
    } else { null }
  }
}
