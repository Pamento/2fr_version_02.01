import { Component, OnInit } from '@angular/core';
import { DataService } from '../../../services/data.service';
import { Router } from '@angular/router';
import { Learner } from 'src/app/entities/learner';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-learner-profil',
  templateUrl: './learner-profil.component.html',
  styleUrls: ['./learner-profil.component.css']
})
export class LearnerProfilComponent implements OnInit {

  learner: Learner;
  _inscriptDate: Date;
  inscriptDate: string;
  test: boolean = true;
  constructor(
    private authService: AuthService,
    private dataService: DataService,
    private Router: Router
  ) { }

  ngOnInit() {
    let options1 = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
    if (this.dataService.learner) {
      this.learner = this.dataService.learner;
      this._inscriptDate = this.dataService.learner.created;
      this.test = this.dataService.learner.test;
    } else if (this.authService.isLoggedIn()) {
      this.learner = JSON.parse(localStorage.getItem('iden'));
      this._inscriptDate = JSON.parse(localStorage.getItem('iden')).created;
      this.test = JSON.parse(localStorage.getItem('iden')).test;
    }
    let date = new Date(this._inscriptDate);
    let newTestDate = new Intl.DateTimeFormat('en-US', options1);
    this.inscriptDate = newTestDate.format(date);
  }
  goHomePage() {
    this.Router.navigate(['/']);
    return false;
  }
  deleteLearner() {
    if (confirm('This action will delete DEFINITELY \n' +
      ' all data from the DATABASE!\n\n' +
      ' Are you sure to\n' +
      ' DELETE your account from the registry ?')) {
      this.dataService.deleteLearner(this.learner);
      this.Router.navigate(['/home']);
    } else { null }
  }
  goToTest() {
    this.Router.navigate(['/test']);
  }
}
