import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LearnerProfilComponent } from './learner-profil.component';

describe('LearnerProfilComponent', () => {
  let component: LearnerProfilComponent;
  let fixture: ComponentFixture<LearnerProfilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LearnerProfilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LearnerProfilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
