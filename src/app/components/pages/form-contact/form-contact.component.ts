import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DataService } from '../../../services/data.service';
import { CompareValidator } from '../../../shered/compar-validator.directive';
import { UserContact } from '../../../entities/user-contact';

@Component({
  selector: 'app-form-contact',
  templateUrl: './form-contact.component.html',
  styleUrls: ['./form-contact.component.css']
})
export class FormContactComponent implements OnInit {
  hide: any;
  formContact: FormGroup;
  constructor(
    private fb: FormBuilder,
    private dataBase: DataService
  ) { }

  /*
   * ngOnInit() this function on the moment of creation of signup_component
   * go to build an reactiv form by createFormRegister() by help of get()
   * the form catch up the values for submit them.
   * 
   * reCAPTCHA tuto for installation
   * https://www.youtube.com/watch?v=8_7zPDr2g4o
   */
  ngOnInit() {
    this.createFormRegister();
  }
  createFormRegister(): any {
    return this.formContact = this.fb.group({
      name: [null, Validators.required],
      email: [null, Validators.compose([Validators.email, Validators.required])],
      message: [null, Validators.required],
      confirmEmail: [null, Validators.compose([Validators.email, Validators.required, CompareValidator('email')])]
    });
  }
  get name() {
    return this.formContact.get('name');
  }
  get email() {
    return this.formContact.get('email');
  }
  get confirmEmail() {
    return this.formContact.get('confirmEmail');
  }
  get message() {
    return this.formContact.get('message');
  }
  submit(): any {
    if (this.formContact.valid) {
      const userMessage: UserContact = Object.assign({}, this.formContact.value);
      delete userMessage.confirmEmail;
      this.dataBase.email(userMessage)
      .subscribe(
        res => {
          console.log('response from Email Sender',res.status); // 200
          console.log('response from Email Sender',res.headers.get('content-type')); // application/json; charset=utf-8
        },
        err => { console.error('Error form contact!',err);}
      );
      // this.formContact.reset();
    }
  }
}
