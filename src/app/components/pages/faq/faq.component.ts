import { Component, OnInit } from '@angular/core';
import { Injectable } from '@angular/core';
import { ScrollToService, ScrollToConfigOptions } from '@nicky-lenaers/ngx-scroll-to';

@Injectable()

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.css']
})
export class FaqComponent implements OnInit {

  constructor(private _scrollToService: ScrollToService) { }

  ngOnInit() { }

  /**
   * to scrolle on the page in to anchor tag element I have use the ngx-scroll-to package
   * https://www.npmjs.com/package/@nicky-lenaers/ngx-scroll-to
   * @param element is taked from faq.component.html on the click event
   */
  scrollToElement(element): void {
    // const targetAnchor = document.querySelector(`#${element}`);
    // if (targetAnchor) targetAnchor.scrollIntoView({ behavior: 'smooth', block: 'start' })

    const config: ScrollToConfigOptions = {
      target: element
    };

    this._scrollToService.scrollTo(config);
  }

}
