import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, ValidationErrors } from '@angular/forms';
import { FrenchTestService } from '../../../services/french-test.service';
import { CheckTestService } from '../../../services/check-test.service';
import { ScrollToService, ScrollToConfigOptions } from '@nicky-lenaers/ngx-scroll-to';
import { Test } from '../../../entities/test';
import { LearnerStorage } from '../../../entities/learnerStorage';
import { ResultOfTest } from '../../../entities/resultOfTest';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {

  learner: LearnerStorage;
  hide: any;
  submitControl: boolean = true;
  formTest: FormGroup;
  errors: Array<string> = [];
  public msg: string;
  totalQuestions: number = 65;
  emptyQuestions: Array<string> = [];
  correctAnswer: number;
  result: ResultOfTest = Object.assign({});
  
  constructor(
    private fb: FormBuilder,
    private dataBase: FrenchTestService,
    private checkTestService: CheckTestService,
    private _scrollToService: ScrollToService
  ) { }

  /*
   * ngOnInit() this function on the moment of creation of signup_component
   * go to build an reactiv form by createFormRegister() by help of get()
   * the form catch up the values for submit them.
   */
  ngOnInit() {
    this.learner = JSON.parse(localStorage.getItem('learner'));
    this.createFormTest();
  }
  createFormTest(): any {
    return this.formTest = this.fb.group({
      question_01: [null, Validators.max(100)],
      question_02: [null, Validators.maxLength(100)],
      question_03: [null, Validators.maxLength(100)],
      question_04: [null, Validators.maxLength(100)],
      question_05: [null, Validators.maxLength(100)],
      question_06: [null, Validators.maxLength(100)],
      question_07: [null, Validators.maxLength(100)],
      question_08: [null, Validators.maxLength(100)],
      question_09: [null, Validators.maxLength(100)],
      question_10: [null, Validators.maxLength(100)],
      question_11: [null, Validators.maxLength(100)],
      question_12: [null, Validators.maxLength(100)],
      question_13: [null, Validators.maxLength(100)],
      question_14: [null, Validators.maxLength(100)],
      question_15: [null, Validators.maxLength(100)],
      question_16: [null, Validators.maxLength(100)],
      question_17: [null, Validators.maxLength(100)],
      question_18: [null, Validators.maxLength(100)],
      question_19: [null, Validators.maxLength(100)],
      question_20: [null, Validators.maxLength(100)],
      question_21: [null, Validators.maxLength(100)],
      question_22: [null, Validators.maxLength(100)],
      question_23: [null, Validators.maxLength(100)],
      question_24: [null, Validators.maxLength(100)],
      question_25: [null, Validators.maxLength(100)],
      question_26: [null, Validators.maxLength(100)],
      question_27: [null, Validators.maxLength(100)],
      question_28: [null, Validators.maxLength(100)],
      question_29: [null, Validators.maxLength(100)],
      question_30: [null, Validators.maxLength(100)],
      question_31: [null, Validators.maxLength(100)],
      question_32: [null, Validators.maxLength(100)],
      question_33: [null, Validators.maxLength(100)],
      question_34: [null, Validators.maxLength(100)],
      question_35: [null, Validators.maxLength(100)],
      question_36: [null, Validators.maxLength(100)],
      question_37: [null, Validators.maxLength(100)],
      question_38: [null, Validators.maxLength(100)],
      question_39: [null, Validators.maxLength(100)],
      question_40: [null, Validators.maxLength(100)],
      question_41: [null, Validators.maxLength(100)],
      question_42: [null, Validators.maxLength(100)],
      question_43: [null, Validators.maxLength(100)],
      question_44: [null, Validators.maxLength(100)],
      question_45: [null, Validators.maxLength(100)],
      question_46: [null, Validators.maxLength(100)],
      question_47: [null, Validators.maxLength(100)],
      question_48: [null, Validators.maxLength(100)],
      question_49: [null, Validators.maxLength(100)],
      question_50: [null, Validators.maxLength(100)],
      question_51: [null, Validators.maxLength(100)],
      question_52: [null, Validators.maxLength(100)],
      question_53: [null, Validators.maxLength(100)],
      question_54: [null, Validators.maxLength(100)],
      question_55: [null, Validators.maxLength(100)],
      question_56: [null, Validators.maxLength(100)],
      question_57: [null, Validators.maxLength(100)],
      question_58: [null, Validators.maxLength(100)],
      question_59: [null, Validators.maxLength(100)],
      question_60: [null, Validators.maxLength(100)],
      question_61: [null, Validators.maxLength(100)],
      question_62: [null, Validators.maxLength(100)],
      question_63: [null, Validators.maxLength(100)],
      question_64: [null, Validators.maxLength(100)],
      question_65: [null, Validators.maxLength(100)],
    });
  }
  get question_01() { return this.formTest.get('question_01'); }
  get question_02() { return this.formTest.get('question_02'); }
  get question_03() { return this.formTest.get('question_03'); }
  get question_04() { return this.formTest.get('question_04'); }
  get question_05() { return this.formTest.get('question_05'); }
  get question_06() { return this.formTest.get('question_06'); }
  get question_07() { return this.formTest.get('question_07'); }
  get question_08() { return this.formTest.get('question_08'); }
  get question_09() { return this.formTest.get('question_09'); }
  get question_10() { return this.formTest.get('question_10'); }
  get question_11() { return this.formTest.get('question_11'); }
  get question_12() { return this.formTest.get('question_12'); }
  get question_13() { return this.formTest.get('question_13'); }
  get question_14() { return this.formTest.get('question_14'); }
  get question_15() { return this.formTest.get('question_15'); }
  get question_16() { return this.formTest.get('question_16'); }
  get question_17() { return this.formTest.get('question_17'); }
  get question_18() { return this.formTest.get('question_18'); }
  get question_19() { return this.formTest.get('question_19'); }
  get question_20() { return this.formTest.get('question_20'); }
  get question_21() { return this.formTest.get('question_21'); }
  get question_22() { return this.formTest.get('question_22'); }
  get question_23() { return this.formTest.get('question_23'); }
  get question_24() { return this.formTest.get('question_24'); }
  get question_25() { return this.formTest.get('question_25'); }
  get question_26() { return this.formTest.get('question_26'); }
  get question_27() { return this.formTest.get('question_27'); }
  get question_28() { return this.formTest.get('question_28'); }
  get question_29() { return this.formTest.get('question_29'); }
  get question_30() { return this.formTest.get('question_30'); }
  get question_31() { return this.formTest.get('question_31'); }
  get question_32() { return this.formTest.get('question_32'); }
  get question_33() { return this.formTest.get('question_33'); }
  get question_34() { return this.formTest.get('question_34'); }
  get question_35() { return this.formTest.get('question_35'); }
  get question_36() { return this.formTest.get('question_36'); }
  get question_37() { return this.formTest.get('question_37'); }
  get question_38() { return this.formTest.get('question_38'); }
  get question_39() { return this.formTest.get('question_39'); }
  get question_40() { return this.formTest.get('question_40'); }
  get question_41() { return this.formTest.get('question_41'); }
  get question_42() { return this.formTest.get('question_42'); }
  get question_43() { return this.formTest.get('question_43'); }
  get question_44() { return this.formTest.get('question_44'); }
  get question_45() { return this.formTest.get('question_45'); }
  get question_46() { return this.formTest.get('question_46'); }
  get question_47() { return this.formTest.get('question_47'); }
  get question_48() { return this.formTest.get('question_48'); }
  get question_49() { return this.formTest.get('question_49'); }
  get question_50() { return this.formTest.get('question_50'); }
  get question_51() { return this.formTest.get('question_51'); }
  get question_52() { return this.formTest.get('question_52'); }
  get question_53() { return this.formTest.get('question_53'); }
  get question_54() { return this.formTest.get('question_54'); }
  get question_55() { return this.formTest.get('question_55'); }
  get question_56() { return this.formTest.get('question_56'); }
  get question_57() { return this.formTest.get('question_57'); }
  get question_58() { return this.formTest.get('question_58'); }
  get question_59() { return this.formTest.get('question_59'); }
  get question_60() { return this.formTest.get('question_60'); }
  get question_61() { return this.formTest.get('question_61'); }
  get question_62() { return this.formTest.get('question_62'); }
  get question_63() { return this.formTest.get('question_63'); }
  get question_64() { return this.formTest.get('question_64'); }
  get question_65() { return this.formTest.get('question_65'); }

  purcent(x,y) {
    return Math.floor(Number((x/y) * 100));
  }
  calculateResult(): ResultOfTest {
    let score = this.checkTestService.doCheck(this.formTest.value);
    this.result.correctAnswer = this.purcent(score, 65);
    this.result.emptyAnswer = this.purcent(this.emptyQuestions.length, 65);
    this.result.total = this.purcent((65 - this.totalQuestions), 65);
    return this.result;
  }
  onChanges(array: Array<string>): void {
    this.formTest.get(array[0]).valueChanges.subscribe(() => {
      if (this.formTest.get(array[0]).valid) {
        array.shift();
        if (array.length == 0) { this.submitControl = true }
      }
    });
  }
  scrollToElement(element): void {
    let elements;
    if (element == Number) {
      elements = ['question_1'];
    } else {
      elements = element;
    }
    const config: ScrollToConfigOptions = {
      target: elements[0]
    };
    this._scrollToService.scrollTo(config);
  }
  sendTest() {
    if (this.formTest.valid) {
      const answers: Object = Object.assign({}, this.formTest.value);
      const newTest: Test = Object.assign({});
      newTest.idUser = this.learner.userId;
      newTest.answers = JSON.stringify(answers);
      newTest.created = new Date();
      newTest.result = JSON.stringify(this.calculateResult());
      this.dataBase.sendTest(newTest).subscribe(
        res => { console.log('TEST ?__', res) },
        err => { console.log('!!! Error from test page ', err) }
      );
      this.formTest.reset();
      this.errors = [];
    }
  }
  confirme(array) {
    if (confirm(this.msg)) {
      this.sendTest();
    } else {
      this.scrollToElement(array);
    }
  }
  submit(): any {
    Object.keys(this.formTest.controls).forEach(key => {
      /**
       * @param value and if condition after is for coutning empty question,
       * that needed before submiting & sending test in to database
       */
      let value = this.formTest.value[key];
      if (value != null) {
        this.totalQuestions -= 1;
      } else {
        this.emptyQuestions.push(key);
      }
      // this condition "if", checks presents of errors of inputs
      const controlErrors: ValidationErrors = this.formTest.get(key).errors;
      if ((controlErrors != null) && this.submitControl) {
        Object.keys(controlErrors).forEach(keyError => {
          this.errors.push(key);
          console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
        });
      }
    });
    this.submitControl = false;
    // if this.errors.length = 0 conditon continue;
    if (this.errors.length) {
      this.msg = 'Please, some text seams to be to long.\nThe max answer lenght is 100 caracters.';
      this.scrollToElement(this.errors);
      this.onChanges(this.errors);
    } else if (this.emptyQuestions.length) {
      this.msg = 'At least one answer is empty.\n\nYou want to send the test anyway ?';
      this.confirme(this.emptyQuestions);
    } else if (this.totalQuestions == 65) {
      this.msg = 'The test is empty.\n\nYou want to send it anyway ?';
      this.confirme(this.totalQuestions);
    } else {
      this.sendTest();
    }
  }
}
