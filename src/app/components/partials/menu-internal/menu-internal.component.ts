import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../../../services/data.service';
import { AuthService } from '../../../services//auth.service';

@Component({
  selector: 'app-menu-internal',
  templateUrl: './menu-internal.component.html',
  styleUrls: ['./menu-internal.component.css']
})
export class MenuInternalComponent implements OnInit {

  isAdmin: boolean = false;
  isLoggedIn: boolean = false;
  isLearner: boolean = false;
  fragment: string;
  constructor(
    private Router: Router,
    private dataService: DataService,
    private authService: AuthService
  ) { }

  ngOnInit() {
    if (this.authService.isAdmin()) {
      console.log('menu InternalPage is admin (?)',this.authService.isAdmin());
      this.isLoggedIn = true;
      this.isAdmin = true;
    } else if (this.authService.isLoggedIn()) {
      this.isLoggedIn = true;
      this.isLearner = true;
    } else {null}
  }
  logout() {
    this.dataService.logOut().subscribe(
      res => {
        localStorage.clear();
        this.Router.navigate(['/home']);
      }
    );
  }
}
