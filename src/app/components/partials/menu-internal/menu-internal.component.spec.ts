import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuInternalComponent } from './menu-internal.component';

describe('MenuInternalComponent', () => {
  let component: MenuInternalComponent;
  let fixture: ComponentFixture<MenuInternalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuInternalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuInternalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
