import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from '../../../services/data.service';
import { AuthService } from '../../../services//auth.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  isAdmin: boolean = false;
  isLoggedIn: boolean = false;
  isLearner: boolean = false;
  fragment: string;
  constructor(
    private route: ActivatedRoute,
    private Router: Router,
    private dataService: DataService,
    private authService: AuthService
  ) { }

  ngOnInit() {
    if (this.authService.isAdmin()) {
      this.isLoggedIn = true;
      this.isAdmin = true;
    } else if (this.authService.isLoggedIn()) {
      this.isLoggedIn = true;
      this.isLearner = true;
    } else {null}
  }
  logout() {
    this.dataService.logOut().subscribe(
      res => {
        localStorage.clear();
        this.Router.navigate(['/home']);
      }
    );
  }
}
