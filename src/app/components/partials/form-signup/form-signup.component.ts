import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { CompareValidator } from '../../../shered/compar-validator.directive';
import { Learner } from '../../../entities/learner';
import { DataService } from '../../../services/data.service';

@Component({
  selector: 'app-form-signup',
  templateUrl: './form-signup.component.html',
  styleUrls: ['./form-signup.component.css']
})
export class FormSignupComponent implements OnInit {

  hide: any;
  formSignup: FormGroup;
  constructor(
    private fb: FormBuilder,
    private dataBase: DataService
  ) { }

  /*
   * ngOnInit() this function on the moment of creation of signup_component
   * go to build an reactiv form by createFormRegister() by help of get()
   * the form catch up the values for submit them.
   */
  ngOnInit() {
    this.createFormRegister();
  }
  createFormRegister():any {
    return this.formSignup = this.fb.group({
      name: [ null, Validators.required ],
      email: [ null, Validators.compose([ Validators.email, Validators.required ])],
      password: [ null, Validators.compose([ Validators.minLength(8), Validators.required ])],
      confirmPassword: [ null, [ Validators.required, CompareValidator('password')] ]
    });
  }
  get name() {
    return this.formSignup.get('name');
  }
  get email() {
    return this.formSignup.get('email');
  }
  get password() {
    return this.formSignup.get('password');
  }
  get confirmPassword() {
    return this.formSignup.get('confirmPassword');
  }
  submit():any {
    /**
     * for change the forme of the date
     * https://stackoverflow.com/questions/3552461/how-to-format-a-javascript-date
     * var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
     * var today  = new Date();
     * console.log(today.toLocaleDateString("en-US")); // 9/17/2016
     * console.log(today.toLocaleDateString("en-US", options)); // Saturday, September 17, 2016
     * console.log(today.toLocaleDateString("hi-IN", options)); // शनिवार, 17 सितंबर 2016
     * You can also use the toLocaleString() method for the same purpose.
     * The only difference is this function provides the time when you don't pass any options.
     * // Example
     * today.toLocaleString()
     * 
     * 9/17/2016, 1:21:34 PM
     */
    if (this.formSignup.valid) {
      const newLearner: Learner = Object.assign({}, this.formSignup.value );
      delete newLearner.confirmPassword;
      newLearner.created = new Date();
      newLearner.last_modification = new Date();
      newLearner.test = false;
      newLearner.isAdmin = false;
      
      this.dataBase.addLearner(newLearner).subscribe(
        (error) => { console.log('!!! Error from addLearner ',error)}
      );
      this.formSignup.reset();
    } else {
      console.error('Invalid form value !'); 
    }
  }
}
