import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { DataService } from '../../../services/data.service';
import { Learner } from '../../../entities/learner';
import { Router } from '@angular/router';

@Component({
  selector: 'app-form-login',
  templateUrl: './form-login.component.html',
  styleUrls: ['./form-login.component.css']
})
export class FormLoginComponent implements OnInit {

  formLogin: FormGroup;
  hide: any;
  constructor(
    private fb: FormBuilder,
    private dataService: DataService,
    private Routes: Router
  ) { }

  ngOnInit() {
    this.loginForm();
  }
  loginForm() {
    return this.formLogin = this.fb.group({
      email: [null, Validators.compose([Validators.email, Validators.required])],
      password: [null, Validators.required]
    });
  }
  get email() {
    return this.formLogin.get('email');
  }
  get password() {
    return this.formLogin.get('password');
  }

  submit() {
    if (this.formLogin.valid) {
      const logLearner: Learner = Object.assign({}, this.formLogin.value);
      this.dataService.login(logLearner)
        .subscribe(
          (data: any) => {
            this.dataService.resLoginData = data,
              localStorage.setItem('token', JSON.stringify(data.id));
            localStorage.setItem('learner', JSON.stringify(data));
            this.dataService.getLearner(data.userId).subscribe(
              learner => {
                if (learner.isAdmin) {
                  this.dataService.admin = learner;
                  localStorage.setItem('admin', JSON.stringify(learner.isAdmin));
                  this.Routes.navigate(['/home']);
                } else {
                  this.dataService.learner = learner;
                  localStorage.setItem('iden', JSON.stringify(learner));
                  if (learner.test) {
                    this.Routes.navigate(['/news']);
                  } else {
                    this.Routes.navigate(['/test']);
                  }
                }
              },
              err => { console.log(err) }
            )
          }
        )
    }
  }
}
