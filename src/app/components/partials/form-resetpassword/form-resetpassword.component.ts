import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DataService } from '../../../services/data.service';
import { Learner } from '../../../entities/learner';

@Component({
  selector: 'app-form-resetpassword',
  templateUrl: './form-resetpassword.component.html',
  styleUrls: ['./form-resetpassword.component.css']
})
export class FormResetpasswordComponent implements OnInit {

  formReset: FormGroup;
  hide: any;
  constructor(
    private fb: FormBuilder,
    private dataService: DataService
  ) { }

  ngOnInit() {
    this.loginForm();
  }
  loginForm() {
    return this.formReset = this.fb.group({
      email: [ null, Validators.compose([ Validators.email, Validators.required ])],
    });
  }
  get email() {
    return this.formReset.get('email');
  }

  submit() {
    if(this.formReset.valid){
      const resetLearnerPass: Learner = Object.assign({}, this.formReset.value);
      this.dataService.resetPassword(resetLearnerPass);
    }
  }
}
