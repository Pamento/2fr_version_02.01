import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LearnerTestComponent } from './learner-test.component';

describe('LearnerTestComponent', () => {
  let component: LearnerTestComponent;
  let fixture: ComponentFixture<LearnerTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LearnerTestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LearnerTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
