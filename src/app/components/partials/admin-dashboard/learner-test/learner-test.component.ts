import { Component, OnInit } from '@angular/core';
import { Learner } from '../../../../entities/learner';
import { DataService } from '../../../../services/data.service';
import { Router } from '@angular/router';
import { FrenchTestService } from '../../../../services/french-test.service';
import { CheckTest } from '../../../../helpers/correct-test'

@Component({
  selector: 'app-learner-test',
  templateUrl: './learner-test.component.html',
  styleUrls: ['./learner-test.component.css']
})
export class LearnerTestComponent implements OnInit {

  learner: Learner;
  testOfLearner: Array<object>;
  test: Array<object> = [];
  // varables for itarations on the CheckTest(model_test) and testOfLearner
  answerCorrect: boolean;
  answer: string;
  index;
  _O;
  constructor(
    private dataService: DataService,
    private Router: Router,
    private testDataBase: FrenchTestService,
  ) {
    this.learner = this.dataService.learner;
    this.testDataBase.getTest(this.learner.id)
      .subscribe(data => {
        this.testOfLearner = JSON.parse(data.answers);
        for (const [key, value] of Object.entries(this.testOfLearner)) {
          this.index = key.split('_');
          this._O = Object.values(CheckTest[parseInt(this.index[1], 10) - 1]);

          for (const i of this._O[2]) {
            if (typeof value === 'string') {
              if (i === value) {
                this.answerCorrect = true;
              } else {
                this.answerCorrect = false;
              }
              this.answer = value;
            } else {
              this.answerCorrect = false;
              this.answer = '... ';
            }
          }
          let itemOfTest = {
            correct: this.answerCorrect,
            question: this._O[1],
            answers: JSON.stringify(this._O[2]),
            learnerAnswer: this.answer,
          }
          this.test.push(itemOfTest);
        }
      },
        err => console.error('Error on learner-test.component', err))
  }

  ngOnInit() {
    // this.createFormTest();
  }
  backToLearnersList() {
    this.Router.navigate(['/daschboard/learner-list']);
  }
  backToLearners() {
    this.Router.navigate(['/daschboard/learner']);
  }
}
