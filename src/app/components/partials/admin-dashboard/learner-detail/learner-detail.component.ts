import { Component, OnInit } from '@angular/core';
import { Learner, LearnerComments } from '../../../../entities/learner';
import { DataService } from '../../../../services/data.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TestResult } from '../../../../entities/test';


@Component({
  selector: 'app-learner-detail',
  templateUrl: './learner-detail.component.html',
  styleUrls: ['./learner-detail.component.css']
})
export class LearnerDetailComponent implements OnInit {

  /**
   * LearnerComments:
   *   level: string;
   *   formula: string;
   *   coursesCounter: number;
   *   comments: string;
  // comments: LearnerComments;
   */
  panelOpenState: boolean = false;
  learner: Learner;
  commentsForm: FormGroup;
  comments: string = '';
  testCreated: any = null;
  _testResult: string = '';
  _comment: string = '';
  _testCreated: Date = null;
  testResult: TestResult;
  constructor(
    private dataService: DataService,
    private Router: Router,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.learner = this.dataService.learner;
    if (typeof this.dataService.test !== 'undefined' || undefined) {
      if (this.dataService.test !== null) {
        this._testResult = this.dataService.test.result;
        this._comment = this.dataService.test.comment;
        this._testCreated = this.dataService.test.created;
        this.populateDashboard();
      } else {
        this.emptyTest();
      }
    } else {
      this.emptyTest();
    }
    this.createCommentsForm();
    this.formValue();
  }

  emptyTest() {
    this.testResult = {
      correctAnswer: 0,
      emptyAnswer: 0,
      total: 0,
    };
  }
  populateDashboard() {
    let options1 = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
    if (this._testResult !== '') {
      // result of test
      let result = JSON.parse(this._testResult);
      this.testResult = result;
      // date of test creation
      let testDate = new Date(this._testCreated);
      let newTestDate = new Intl.DateTimeFormat('fr-FR', options1);
      this.testCreated = newTestDate.format(testDate);
    } else {
      this.testResult = null;
      this.testCreated = null;
    }
    if (this._comment !== null) {
      // comment about learner from test mysql table
      let comment = JSON.parse(this._comment);
      this.comments = comment;
    } else {
      this.comments = '';
    }
  }
  createCommentsForm(): any {
    return this.commentsForm = this.fb.group({
      learnerNotes: [null, Validators.required]
    });
  }
  formValue() {
    this.commentsForm.controls['learnerNotes'].setValue(this.comments || '');
  }
  get learnerNotes() {
    return this.commentsForm.get('learnerNotes');
  }
  submit(): any {
    if (this.commentsForm.valid) {
      const learComs: string = Object.assign({}, this.commentsForm.value);
      this.comments = learComs;
      this.learner.comment = JSON.stringify(this.comments);
      this.dataService.updateLearner(this.learner)
        .subscribe(
          res => {
            console.log('response from Email Sender', res.status); // 200
            console.log('response from Email Sender', res.headers.get('content-type')); // application/json; charset=utf-8
            this.Router.navigate(['/daschboard/learner-list']);
          },
          err => { console.error(err) }
        );
    }
  }
  backToLearnersList() {
    this.dataService.test = null;
    this.Router.navigate(['/daschboard/learner-list']);
  }
  deleteLearner() {
    if (confirm('Cette action va suprimer DEFINITIVEMENT\n' +
      'tout les données de la BASE DE DONNÉES!\n\n' +
      'Êtes-vous sur de vouloire\n' +
      'SUPRIMER l\'apprenti du régistre ?')) {
      this.dataService.deleteLearner(this.learner);
      this.Router.navigate(['/daschboard/learner-list']);
    } else { null }

  }
}
