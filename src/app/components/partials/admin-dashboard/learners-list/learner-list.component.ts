import { Component, OnInit } from '@angular/core';
import { DataService } from '../../../../services/data.service';
import { Router } from '@angular/router';
import { Learner } from '../../../../entities/learner';
import { FrenchTestService } from '../../../../services/french-test.service';

@Component({
  selector: 'app-learner-list',
  templateUrl: './learner-list.component.html',
  styleUrls: ['./learner-list.component.css']
})
export class LearnerListComponent implements OnInit {

  learners: Array<Learner> = [];
  constructor(
    private dataService: DataService,
    private frenchTestService: FrenchTestService,
    private Router: Router,
  ) { }

  ngOnInit() {
    let options1 = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };

    this.dataService.getLearners()
      .subscribe((data) => {
        data.forEach(e => {
          if (e.isAdmin !== true) {
            let createdDate = new Date(e.created);
            let newCreatedDAte = new Intl.DateTimeFormat('fr-FR', options1);
            e['createdAd'] = newCreatedDAte.format(createdDate);
            this.learners.push(e)
          } else { null }
        });
      });
  }
  showLearner(learnerId:number) {
    this.learners.forEach( l => {
      if(l.id === learnerId) {
        this.dataService.learner = l;
        if(l.test) {
          this.frenchTestService.getTest(l.id)
          .subscribe(
            data => {
              this.dataService.test = data;
              this.Router.navigate(['/daschboard/learner']);
            },
            err => { console.log('Error in form login for getTest',err);
            });
          } else {
            this.Router.navigate(['/daschboard/learner']);
          }
      } else { null }
    });
  }
}
