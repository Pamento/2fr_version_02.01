favicon source
158644305_de6daf528a_m.jpg (name before change to favicon.ico)
https://www.flickr.com/photos/lwr/158644305

RGPD
https://www.cnil.fr/fr/professionnels-comment-repondre-une-demande-de-droit-dacces

/**
 * for change the forme of the date
 * https://stackoverflow.com/questions/3552461/how-to-format-a-javascript-date
 * var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
 * var today  = new Date();
 * console.log(today.toLocaleDateString("en-US")); // 9/17/2016
 * console.log(today.toLocaleDateString("en-US", options)); // Saturday, September 17, 2016
 * console.log(today.toLocaleDateString("hi-IN", options)); // शनिवार, 17 सितंबर 2016
 * You can also use the toLocaleString() method for the same purpose.
 * The only difference is this function provides the time when you don't pass any options.
 * // Example
 * today.toLocaleString()
 * 
 * 9/17/2016, 1:21:34 PM
 */